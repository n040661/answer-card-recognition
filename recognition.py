import cv2
import numpy as np
def 定位点识别(image):
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    thresh = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]
    cnts,_ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    questionCnts = []
    for c in cnts:
        (x, y, w, h) = cv2.boundingRect(c) # 计算轮廓的边界框
        ar = w / float(h)# 计算宽高比
    
        # 轮廓是气泡->边至少是20个像素，且宽高比近似为1
        if w >= 20 and h >= 20 and ar >= 0.9 and ar <= 1.1:
            questionCnts.append(c)# 存储气泡轮廓
    circle_x=[]
    circle_y=[]
    avg_list=[]
    for i in questionCnts:
        for a in i:
            circle_x.append(a[0][0])
            circle_y.append(a[0][1])
        avg_x=sum(circle_x)/len(circle_x)
        avg_y=sum(circle_y)/len(circle_y)
        circle_x=[]
        circle_y=[]
        avg_list.append((int(avg_x),int(avg_y)))
        # cv2.drawContours(image,questionCnts , -10,(0, 0, 255),3)
    image_shape=image.shape
    sp=((0,0),(image_shape[1],0),(image_shape[1],image_shape[0]),(0,image_shape[0]))
    s四角=[]
    for i in sp:
        min_距离=[]

        for a in avg_list:
            min_距离.append((abs((a[0]-i[0])**2)+abs((a[1]-i[1])**2))**0.5)
        min_下标=min_距离.index(min(min_距离))
        s四角.append(avg_list[min_下标])
    return s四角
def 四点透视变换(point1,image):
    # old_shape=image.shape
    for i in point1:
        cv2.circle(image, i, 10, (0, 0, 255) , -1)
    point2 = np.array([[0,0],[point1[1][0]-point1[0][0],0],[point1[1][0]-point1[0][0],point1[2][1]-point1[1][1]],[0,point1[2][1]-point1[1][1]]],dtype = "float32")
    point=np.array(point1,dtype = "float32")
    M = cv2.getPerspectiveTransform(point,point2)
    out_img = cv2.warpPerspective(image,M,(point1[1][0]-point1[0][0],point1[2][1]-point1[1][1]))
    return out_img
	